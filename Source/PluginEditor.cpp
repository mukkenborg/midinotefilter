/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
MidinoteFilterAudioProcessorEditor::MidinoteFilterAudioProcessorEditor (MidinoteFilterAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    addAndMakeVisible (actionGroupComponent = new ChoiceParameterGroupComponent (*p.actionParam));
    addAndMakeVisible (labelNote = new Label ("labelNote",
                                              TRANS("MIDI Note")));
    labelNote->setFont (Font (15.00f, Font::plain));
    labelNote->setJustificationType (Justification::centredLeft);
    labelNote->setEditable (false, false, false);
    labelNote->setColour (TextEditor::textColourId, Colours::black);
    labelNote->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (sliderMidiNote = new IntParameterSlider (*p.noteParam));
    sliderMidiNote->setTooltip (TRANS("MIDI note to filter"));    
    sliderMidiNote->setSliderStyle (Slider::LinearHorizontal);
    sliderMidiNote->setTextBoxStyle (Slider::TextBoxBelow, true, 80, 20);
    sliderMidiNote->setTextBoxIsEditable(true);

    setSize (253, 120);
}

MidinoteFilterAudioProcessorEditor::~MidinoteFilterAudioProcessorEditor()
{
    actionGroupComponent = nullptr;
    labelNote = nullptr;
    sliderMidiNote = nullptr;
}

//==============================================================================
void MidinoteFilterAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (Colours::white);
}

void MidinoteFilterAudioProcessorEditor::resized()
{
    actionGroupComponent->setBounds (123, 10, 120, 100);
    labelNote->setBounds (10, 33, 96, 24);
    sliderMidiNote->setBounds (16, 72, 80, 32);
}
