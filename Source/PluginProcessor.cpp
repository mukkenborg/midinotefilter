/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
MidinoteFilterAudioProcessor::MidinoteFilterAudioProcessor() : noteParam(nullptr),
                                                               actionParam(nullptr)
{
    addParameter(noteParam = new AudioParameterInt("MIDINOTEFILTER_PARAM_NOTE", "MIDI note", 0, 127, 36));
    addParameter(actionParam = new AudioParameterChoice("MIDINOTEFILTER_PARAM_ACTION", "Filter action", {"Pass", "Drop"}, 0));
    filterActionPass = true;
}

MidinoteFilterAudioProcessor::~MidinoteFilterAudioProcessor()
{
    noteParam = nullptr;
    actionParam = nullptr;
}

//==============================================================================
const String MidinoteFilterAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool MidinoteFilterAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool MidinoteFilterAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double MidinoteFilterAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int MidinoteFilterAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int MidinoteFilterAudioProcessor::getCurrentProgram()
{
    return 0;
}

void MidinoteFilterAudioProcessor::setCurrentProgram (int index)
{
}

const String MidinoteFilterAudioProcessor::getProgramName (int index)
{
    return String();
}

void MidinoteFilterAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void MidinoteFilterAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void MidinoteFilterAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

//#ifndef JucePlugin_PreferredChannelConfigurations
//bool MidinoteFilterAudioProcessor::setPreferredBusArrangement (bool isInput, int bus, const AudioChannelSet& preferredSet)
//{
//    // Reject any bus arrangements that are not compatible with your plugin
//
//    const int numChannels = preferredSet.size();
//
//   #if JucePlugin_IsMidiEffect
//    if (numChannels != 0)
//        return false;
//   #elif JucePlugin_IsSynth
//    if (isInput || (numChannels != 1 && numChannels != 2))
//        return false;
//   #else
//    if (numChannels != 1 && numChannels != 2)
//        return false;
//
//    if (! AudioProcessor::setPreferredBusArrangement (! isInput, bus, preferredSet))
//        return false;
//   #endif
//
//    return AudioProcessor::setPreferredBusArrangement (isInput, bus, preferredSet);
//}
//#endif

bool MidinoteFilterAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
    // Only mono/stereo and input/output must have same layout
    const AudioChannelSet& mainOutput = layouts.getMainOutputChannelSet();

    // input and output layout must be the same
    if (layouts.getMainInputChannelSet() != mainOutput)
        return false;

    // do not allow disabling the main buses
    if (mainOutput.isDisabled()) return false;

    // only allow stereo and mono
    if (mainOutput.size() > 2) return false;

    return true;
}

void MidinoteFilterAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    filterActionPass = !(actionParam->getIndex());
    buffer.clear();
     
    MidiBuffer processedMidi;
    int time;
    MidiMessage m;
    
    for (MidiBuffer::Iterator i (midiMessages); i.getNextEvent (m, time);)
    {
        if( m.isNoteOn() || m.isNoteOff() || m.isAftertouch())
        {
            if( m.getNoteNumber() == noteParam->get() )
            {
                if( filterActionPass )
                        processedMidi.addEvent(m, time);
            }
            else
            {
                if( !filterActionPass )
                    processedMidi.addEvent(m, time);
            }
        }
        else
            processedMidi.addEvent(m, time);
    }
    midiMessages.swapWith (processedMidi); 
}

//==============================================================================
bool MidinoteFilterAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* MidinoteFilterAudioProcessor::createEditor()
{
    return new MidinoteFilterAudioProcessorEditor (*this);
}

//==============================================================================
void MidinoteFilterAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    // Create an outer XML element..


    XmlElement xml ("MIDINOTEFILTERSETTINGS");

    // add some attributes to it..
    //xml.setAttribute ("uiWidth", lastUIWidth);
    //xml.setAttribute ("uiHeight", lastUIHeight);

    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i)
    {
        if (AudioParameterInt* p = dynamic_cast<AudioParameterInt*> (getParameters().getUnchecked(i)))
            xml.setAttribute (p->paramID, p->get());
        if (AudioParameterChoice* p = dynamic_cast<AudioParameterChoice*> (getParameters().getUnchecked(i)))
            xml.setAttribute (p->paramID, p->getIndex());
    }
    // then use this helper function to stuff it into the binary blob and return it..
    copyXmlToBinary (xml, destData);
}

void MidinoteFilterAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    ScopedPointer<XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

    if (xmlState != nullptr)
    {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName ("MIDINOTEFILTERSETTINGS"))
        {
            // ok, now pull out our last window size..
            //lastUIWidth  = jmax (xmlState->getIntAttribute ("uiWidth", lastUIWidth), 400);
            //lastUIHeight = jmax (xmlState->getIntAttribute ("uiHeight", lastUIHeight), 200);

            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
            {
                if (AudioParameterInt* p = dynamic_cast<AudioParameterInt*> (getParameters().getUnchecked(i)))
                    *p = (xmlState->getIntAttribute (p->paramID, p->get()));
                if (AudioParameterChoice* p = dynamic_cast<AudioParameterChoice*> (getParameters().getUnchecked(i)))
                {
                    *p = (xmlState->getIntAttribute (p->paramID, p->getIndex()));
                    filterActionPass = !(p->getIndex());
                }
            }
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new MidinoteFilterAudioProcessor();
}
