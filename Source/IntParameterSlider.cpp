/*
  ==============================================================================

    IntParameterSlider.cpp
    Created: 28 Sep 2016 4:13:44pm
    Author:  mikkle

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "IntParameterSlider.h"

//==============================================================================
IntParameterSlider::IntParameterSlider (AudioParameterInt& p) : Slider (((AudioProcessorParameter&)p).getName (256)), param (p)
{
    setRange (param.getRange().getStart(), param.getRange().getEnd(), 1);
    startTimerHz (30);
    updateSliderPos();
}

IntParameterSlider::~IntParameterSlider()
{
}

void IntParameterSlider::valueChanged()
{
    param = (int)Slider::getValue();
}

void IntParameterSlider::timerCallback()
{
    updateSliderPos();
}

void IntParameterSlider::startedDragging()
{
    param.beginChangeGesture();
}

void IntParameterSlider::stoppedDragging()
{
    param.endChangeGesture();
}

void IntParameterSlider::updateSliderPos()
{
    const int newValue = param.get();
    if (newValue != (int) Slider::getValue() && ! isMouseButtonDown())
        Slider::setValue (newValue);
}

