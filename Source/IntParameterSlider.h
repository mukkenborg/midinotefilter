/*
  ==============================================================================

    IntParameterSlider.h
    Created: 28 Sep 2016 4:13:43pm
    Author:  mikkle

  ==============================================================================
*/

#ifndef INTPARAMETERSLIDER_H_INCLUDED
#define INTPARAMETERSLIDER_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class IntParameterSlider    : public Slider,
                              private Timer
{
public:
    IntParameterSlider (AudioParameterInt&);
    ~IntParameterSlider();

private:
    void valueChanged() override;
    void timerCallback() override;
    void startedDragging() override;
    void stoppedDragging() override;

    AudioParameterInt& param;
    void updateSliderPos();
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (IntParameterSlider)
};

#endif  // INTPARAMETERSLIDER_H_INCLUDED
