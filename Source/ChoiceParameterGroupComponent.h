/*
  ==============================================================================

    ChoiceParameterGroupComponent.h
    Created: 28 Sep 2016 10:49:46pm
    Author:  mikkle

  ==============================================================================
*/

#ifndef CHOICEPARAMETERGROUPCOMPONENT_H_INCLUDED
#define CHOICEPARAMETERGROUPCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class ChoiceParameterGroupComponent    : public GroupComponent,
                                         public Button::Listener,
                                         private Timer
{
public:
    ChoiceParameterGroupComponent(AudioParameterChoice&);
    ~ChoiceParameterGroupComponent();

private:
    void timerCallback() override;
    void resized() override;
    void buttonClicked (Button* button) override;
    
    void updateToggleButtonStates();
    AudioParameterChoice& param;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ChoiceParameterGroupComponent)
};

#endif  // CHOICEPARAMETERGROUPCOMPONENT_H_INCLUDED
