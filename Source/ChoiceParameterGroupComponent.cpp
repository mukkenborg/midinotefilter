/*
  ==============================================================================

    ChoiceParameterGroupComponent.cpp
    Created: 28 Sep 2016 10:49:46pm
    Author:  mikkle

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "ChoiceParameterGroupComponent.h"

//==============================================================================
ChoiceParameterGroupComponent::ChoiceParameterGroupComponent(AudioParameterChoice& p) : GroupComponent(((AudioProcessorParameter&)p).getName (256),((AudioProcessorParameter&)p).getName (256)), param(p)
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
    ToggleButton* tb;
    for( int i = 0; i < param.choices.size(); i++ )
    {
        addAndMakeVisible (tb = new ToggleButton (param.choices[i]));
        tb->setComponentID(param.choices[i]);
        tb->setRadioGroupId(1, NotificationType::sendNotification);
        tb->addListener(this);
    }
    tb = nullptr;
    
    updateToggleButtonStates();
    startTimerHz (30);
}

ChoiceParameterGroupComponent::~ChoiceParameterGroupComponent()
{
    for( int i = 0; i <getNumChildComponents(); i++ )
    {
        ((ToggleButton*)getChildComponent(i))->removeListener(this);
    }
    deleteAllChildren ();
}

void ChoiceParameterGroupComponent::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..
    int x = 24;
    int y = 24;
    int w = 72;
    int h = 24;
    int s = 33;
    for(int i = 0; i < getNumChildComponents(); i++ )
    {
        getChildComponent(i)->setBounds(x,y,w,h);
        y += s;
    }
}

void ChoiceParameterGroupComponent::timerCallback()
{
    updateToggleButtonStates();
}

void ChoiceParameterGroupComponent::buttonClicked(Button* button)
{
    const int idx = getIndexOfChildComponent(button);
    if( idx != param.getIndex() )
        param = idx;
}

void ChoiceParameterGroupComponent::updateToggleButtonStates()
{
    const int i = param.getIndex();
    if( !((ToggleButton*)getChildComponent(i))->getToggleState())
        ((ToggleButton*)getChildComponent(i))->setToggleState(true, NotificationType::sendNotification);
}
